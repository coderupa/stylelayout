import React, { Component } from 'react'
import { View,Text,Dimensions, StyleSheet,Image } from 'react-native'
import {Container,Header,Content} from '../components'

const {width,height} = Dimensions.get('window')

export default class Dimension extends Component {
  state = {
    videoWidth:null,
    videoHeight:null,
    orientation:null
    }

  _getOrientation(width,height){
    return width < height ? 'Portrait' :'Landscape'
  }
  _updateLayout(event){
    const {width,height} = event.nativeEvent.layout
    let orientation = this._getOrientation(width,height)
    let vW = width - 20
    let vH = 0.6 * vW
    if(orientation =='Landscape'){
      vW = 0.3 * (width-20)
      vH = 0.6 * vW
    }
    this.setState({
      videoWidth:vW,
      videoHeight:vH,
      orientation:orientation
    })
  }
  render() {
    let videoInfoStyle=styles['videoInfo' + this.state.orientation]
    return (
      <Container style={styles.container} onLayout={event => this._updateLayout(event)}>
        <Header title='Youtube Screen'/>
        <Content>
          <View style={styles.videoWrapper}>
            <Image style={{width:this.state.videoWidth,height:this.state.videoHeight}} source={require('../assets/video-thumbnail.png')}/>
            <View style={[styles.videoInfo,videoInfoStyle]}>
              <Image style={styles.videoLogo} source={require('../assets/video-logo.png')}/>
              <View>
                <Text style={styles.videoTitle}> This is Video title #1</Text>
                <Text style={styles.videoMeta}> Channel #1 · 205k view · 19 hours ago</Text>
              </View>
            </View>
          </View>
          <View style={styles.videoWrapper}>
            <Image style={{width:this.state.videoWidth,height:this.state.videoHeight}} source={require('../assets/video-thumbnail.png')}/>
            <View style={[styles.videoInfo,videoInfoStyle]}>
              <Image style={styles.videoLogo} source={require('../assets/video-logo.png')}/>
              <View>
                <Text style={styles.videoTitle}> This is Video title #2</Text>
                <Text style={styles.videoMeta}> Channel #1 · 205k view · 19 hours ago</Text>
              </View>
            </View>
          </View>
          <View style={styles.videoWrapper}>
            <Image style={{width:this.state.videoWidth,height:this.state.videoHeight}} source={require('../assets/video-thumbnail.png')}/>
            <View style={[styles.videoInfo,videoInfoStyle]}>
              <Image style={styles.videoLogo} source={require('../assets/video-logo.png')}/>
              <View>
                <Text style={styles.videoTitle}> This is Video title #3</Text>
                <Text style={styles.videoMeta}> Channel #1 · 205k view · 19 hours ago</Text>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    )
  }
}


const styles=StyleSheet.create({
  videoWrapper:{
    padding:10,
    marginBottom:10,
    borderBottomWidth:1,
    borderBottomColor:'#CCC',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  videoInfo:{
    marginTop:5,
    padding:5,
       
  },
  videoInfoPortrait:{
    flexDirection:"row" 
  },
  videoInfoLandscape:{
   flexDirection:"column-reverse",
   justifyContent:"flex-end",
   marginLeft:10
  },
  videoLogo:{
    width:40,
    height:40,
    marginRight:10
  },
  videoTitle:{
    fontSize:16
  },
  videoMeta:{
    color:'#777'
  }
})