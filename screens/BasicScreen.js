import React, { Component } from 'react' 
import { View,Text } from 'react-native' 
import {Container,Header,Content,Footer} from '../components'
export default() =>(
  <Container>
    <Header title="Header"/>
    <Content>
      <Text>Content of Basic Screen</Text>
    </Content>
    <Footer>
      <Text>Footer</Text>
    </Footer>
  </Container>
)


