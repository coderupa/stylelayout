import React, { Component } from 'react'
import { View,Text,StyleSheet,Image,TouchableOpacity } from 'react-native'
import {Container,Button} from '../components'
export default class Onboarding extends Component {
  state = {  }
  render() {
    return (
      <Container>
        <Image style={styles.coverImage} source={require('../assets/onboarding_cover.png')}>
          <Text style={styles.title}>Backed up safely</Text>
          <Image style={styles.mainImage} source={require('../assets/onboarding_main.png')}/>
          <Button title="GET STARTED" onPress={()=>null}/>
          <Text style={styles.description}> Google Photos supports face matching technology Learn More </Text>
        </Image>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  title:{
    backgroundColor:'transparent',
    fontSize:20,
    color:'#FFF',
    fontWeight:'500',
    marginBottom:40
  },
  coverImage:{
    flex:1,
    width: undefined,
    height: undefined,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainImage:{

  },
  button:{
    paddingVertical:10,
    paddingHorizontal:60,
    backgroundColor:'#FFF',
    marginTop:20,
    shadowColor: "#000",
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.2,
		shadowRadius:  1,
  },
  buttonCaption:{
    fontWeight:'500',
    fontSize:12
  },
  description:{
    backgroundColor:'transparent',
    fontSize:14,
    color:'#FFF',
    fontWeight:'500',
    padding:20,
    textAlign:"center"
  }
})