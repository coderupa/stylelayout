import React, { Component } from 'react'
import { View,Text,Dimensions, StyleSheet } from 'react-native'
const {width,height} = Dimensions.get('window')

export default class Dimension extends Component {
  state = {
    screenWidth:width,
    screenHeight:height,
    orientation:this._getOrientation(width,height)
    }

  _getOrientation(width,height){
    return width < height ? 'Portrait' :'Landscape'
  }
  _updateLayout(event){
    const {width,height} = event.nativeEvent.layout
    let orientation = this._getOrientation(width,height)
    this.setState({
      screenWidth:width,
      screenHeight:height,
      orientation:orientation
    })
  }
  render() {
    return (
      <View style={styles.container} onLayout={event => this._updateLayout(event)}>
        <Text style={styles.text}>Orientation : {this.state.orientation}</Text>
        <Text style={styles.text}>Width :{this.state.screenWidth}</Text>
        <Text style={styles.text}>Height : {this.state.screenHeight}</Text>
      </View>
    )
  }
}


const styles=StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#FFF',
    justifyContent:'center',
    alignItems:'center'
  },
  text:{
    fontSize:30
  }
})