import React, { Component } from 'react'
import { View,Text,StyleSheet } from 'react-native'
import xStyles from'../styles'
/*
- inline style
- local style
- external style 
*/
export default class BasicStyles extends Component {
  state = {  }
  render() {
    return (
      <View>
        <Text style={[styles.headline,styles.centerText]}>React Native Style & Layout</Text>
        <Text style={[styles.subHeadline,styles.centerText,xStyles.border]}>Learning how to styling in React Native</Text>
      </View>
    )
  }
}

const styles=StyleSheet.create({
  headline:{
    fontSize:20,
    marginTop:40,
    color:'rgb(255,0,255)'
  },
  subHeadline:{
    fontWeight:'bold'
  },
  centerText:{
    textAlign:'center'
  }
})