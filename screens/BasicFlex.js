import React, { Component } from 'react'
import { View,Text,StyleSheet } from 'react-native'

/*
	•	flex √ 
	•	flexDirection √   
	•	alignItems √
	•	justifyContent √
	•	alignSelf √
	•	flexWrap √
*/

export default class BasicFlex extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.box1}>
          <Text>Box1</Text>
        </View>
        <View style={styles.box2}>
          <View style={[styles.item,{alignSelf:'flex-start'}]}></View>
          <View style={[styles.item,{alignSelf:'center'}]}></View>
          <View style={[styles.item,{alignSelf:'flex-end'}]}></View>
        </View>
        <View style={styles.box3}>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
        </View>
      </View>      
    )
  }
}

const styles=StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#CCC'
  },
  box1:{
    flex:2, // 1:6
    backgroundColor:'red'
  },
  box2:{
    flex:4, // 4:6
    backgroundColor:'blue',
    flexDirection:'column', // vertical axis ↧
    // justifyContent :'center', // sesuai axis
    justifyContent: 'space-between', // flex-start | flex-end | center | space-around | space-between
    alignItems:'center', // kebalikan axis ↦
    alignItems:'flex-start' //flex-start | flex-end | center | baseline | stretch
  },
  box3:{
    flex:2, // 1:6
    backgroundColor:'green',
    flexDirection:'row', // horizontal axis ↦
    justifyContent :'center',
    alignItems:'center',
    flexWrap:'wrap'
    
  },
  item:{
    height:50,
    width:50,
    margin:2,
    borderWidth:2,
    borderColor:'#000',
    backgroundColor:'#FFF'
  }
})