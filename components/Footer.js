import React, { Component } from 'react'
import { View,Text,StyleSheet } from 'react-native'

export default ({children}) => (
  <View style={styles.footer}>
    {children}
  </View>
)

const styles=StyleSheet.create({
  footer:{
    height:50,
    backgroundColor:'#F8F8F8',
    justifyContent:'center'
  }
})