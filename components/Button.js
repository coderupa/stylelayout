import React, { Component } from 'react'
import { Text,StyleSheet,TouchableOpacity } from 'react-native'

export default ({title,onPress}) => (
  <TouchableOpacity style={styles.button} onPress={onPress}>
    <Text style={styles.buttonCaption}>{title}</Text>
  </TouchableOpacity>
)

const styles=StyleSheet.create({
  button:{
    paddingVertical:10,
    paddingHorizontal:60,
    backgroundColor:'#FFF',
    marginTop:20,
    shadowColor:'#000',
    shadowOffset:{width:2,height:2},
    shadowOpacity:0.2,
    shadowRadius:1
  },
  buttonCaption:{
    fontWeight:'500',
    fontSize:12
  }
})