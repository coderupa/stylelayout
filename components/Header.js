import React, { Component } from 'react'
import { View,Text,StyleSheet,Platform } from 'react-native'
const platform = Platform.OS // Platform Spesific Code

export default ({title}) => (
  <View style={styles.header}>
    <Text style={styles.headerText}>{title}</Text>
  </View>
)

const styles=StyleSheet.create({
  header:{
    height:64,
    paddingTop:15,
    justifyContent:'center',
    backgroundColor:platform ==='ios' ? '#F8F8F8':'darkslateblue'
  },
  headerText:{
    fontSize:15,
    fontWeight:'bold',
    textAlign:'center',
    color:platform =='ios' ?'#000' :'#FFF'
  }
})