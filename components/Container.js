import React, { Component } from 'react'
import { View,Text,StyleSheet } from 'react-native'

export default ({children,onLayout}) => (
  <View onLayout={onLayout} style={styles.container}>
    {children}
  </View>
)

const styles=StyleSheet.create({
  container:{
    flex:1
  }
})