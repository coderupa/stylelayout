import React, { Component } from 'react'
import { View,Text,StyleSheet, ScrollView } from 'react-native'

export default ({children}) => (
  <ScrollView style={styles.content}>
    {children}
  </ScrollView>
)

const styles=StyleSheet.create({
  content:{
    flex:1
  }
})